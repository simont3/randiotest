# The MIT License (MIT)
#
# Copyright (c) 2018-2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import traceback

from . import Executor
from .tools import parseargs

def main():
    opts = parseargs()

    p = Executor(opts.script)

    try:
        with open(opts.script, 'r') as shdl:
            exec(shdl.read())
        p.close('Finished')
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        e = traceback.format_exception(exc_type, exc_value, exc_traceback)

        line = msg = ''
        for i in zip(e, range(len(e))):
            if i[0].strip().startswith('File "<string>"'):
                line = e[i[1]].split(',')[1].strip()
                msg = e[-1]
                break

        sys.exit('\nFatal:\tscript "{}" failed:'
                 '\n\thint: {}\n\t       {}'
                 .format(opts.script, line, msg))

if __name__ == '__main__':
    main()
