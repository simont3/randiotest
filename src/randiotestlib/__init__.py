# The MIT License (MIT)
#
# Copyright (c) 2018-2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import traceback
from os import stat, SEEK_SET
from time import time, perf_counter_ns
from random import randrange
from math import cos, sin, pi
from csv import DictWriter

from .orderedset import OrderedSet


class Executor():
    """
    Provide an environment in which test methods can be executed against a file.

    A virtual cursor is used to define the cell to be read as next.
    """

    def __init__(self, script):
        """
        :param script: the filename of the test procedure script
        """
        self.script = script.split('.')[0]
        self.initialized = False
        self._verbose = False
        self.KB = 1024
        self.MB = 2**20

    def setup(self, file='path/to/datafile', rows=128, cols=128):
        """
        Setup the Executor().

        The file will be interpreted as a number of cells, addressed by rows and columns, where the
        cell size is defined by the number of cells asked for:

        ..  math::

            file size / (rows x columns) = cell size

        :param file:  the file to be read
        :param rows:  the number of rows into which the file shall be split
        :param cols:  ...and the columns
        """
        self.infile = file
        self.logfile = open(f'{self.script}.{int(time())}.log', 'w')
        self.csvfile = open(f'{self.script}.{int(time())}.csv', 'w', newline='')
        # 40,913 bytes from  82/ 81 (byte   546,802,245) -->   0.001006 secs,    38.780 MB/sec
        self.csvwriter = DictWriter(self.csvfile, ['comment', 'bytes', 'row', 'col', 'startpos', 'duration', 'MB/sec'])
        self.csvwriter.writeheader()
        self.infilesize = stat(self.infile).st_size
        self.inhdl = open(self.infile, 'rb')
        self.rows = rows  # no. of rows
        self.cols = cols  # no. of cols
        self._row = 0  # actual row
        self._col = 0  # actual col
        self.cellsize = int(self.infilesize / (self.rows*self.cols))
        self.comment(f'Running: {self.script}\n')

        # format for printing the results of a read, should look like this:
        # 104,857,600 bytes from  0/ 0 (byte           0 -   0.069964 secs
        # (taking care of the file/row/col sizes for proper formatting)
        _f = '{}{}{}{}{}{}{}{}{}'.format('\t{:',
                                         len('{:,}'.format(self.infilesize)),
                                         ',} bytes from {:',
                                         len('{:,}'.format(self.rows)), '}/{:',
                                         len('{:,}'.format(self.cols)),
                                         '} (byte {:',
                                         len('{:,}'.format(self.infilesize)),
                                         ',}) --> {:>10.6f} secs, {:9.3f} MB/sec')
        self.readformat = _f

        self.initialized = True

        txt = f'\tsetup: file     = {self.infile}\n' \
              f'\t       filesize = {self.infilesize:,} bytes\n' \
              f'\t       rows     = {self.rows}\n' \
              f'\t       cols     = {self.cols}\n' \
              f'\t       cellsize = {self.cellsize:,} bytes'
        self._print(txt)
        self._print()
        self.csvwriter.writerow({'comment': txt,
                                 'bytes': '',
                                 'row': '',
                                 'col': '',
                                 'startpos': '',
                                 'duration': '',
                                 'MB/sec': ''
                                 })

    def verbose(self, mode=True):
        """
        Enable verbose (chatty) mode.

        :param mode:  boolean to control verbose output
        """
        self._verbose = mode
        self._talkverbose(f'\tverbose: {self._verbose}')

    def comment(self, *args):
        """
        Print a comment.

        :param *args:    the comment to print
        """
        self._print(*args)
        self.csvwriter.writerow({'comment': ' '.join([str(x) for x in args]),
                                 'bytes': '',
                                 'row': '',
                                 'col': '',
                                 'startpos': '',
                                 'duration': '',
                                 'MB/sec': ''
                                 })


    def pos(self, row=None, col=None):
        """
        Position the virtual cursor.

        :param row:  the row to position the cursor to
        :param col:  ...and the column
        """
        if row < 0 or row > self.rows-1:
            raise ValueError(f'row {row} is out of range (0 - {self.rows-1}')
        if col < 0 or col > self.cols - 1:
            raise ValueError(f'col {col} is out of range (0 - {self.cols-1}')

        self._row = row
        self._col = col
        self._talkverbose(f'\tpos:   row = {self._row}, col = {self._col}')


    def pos_random(self, row_distance=0, col_distance=0):
        """
        Randomly position the virtual cursor.

        :param row_distance:  min. distance to the borders (vertically)
        :param col_distance:  min. distance to the borders (horizontally)
        """
        self._row = randrange(row_distance, self.rows-1-row_distance)
        self._col = randrange(col_distance, self.cols-1-col_distance)

        self._talkverbose(f'\tpos_random:   row = {self._row}, col = {self._col}')


    def read(self, size=None, fromend=False):
        """
        Read <size> bytes from wherever the virtual cursor is positioned.

        :param size:     the amount of bytes to read (or ``None`` for cell size)
        :param fromend:  option to read from the end of the file, skips the
                         actual position
        :return:         a tuple (row, col, byte_startpos, no. of bytes read, duration)
        """
        _size = size if size else self.cellsize
        _size = int(_size)
        if not fromend:
            _pos = self._row * (self.rows * self.cellsize) + \
                   self._col * self.cellsize

            self.inhdl.seek(_pos, SEEK_SET)
        else:
            _pos = self.infilesize - _size
            self._row = self.rows-1
            self._col= self.cols-1
            self.inhdl.seek(_size, SEEK_SET)
        _start = perf_counter_ns()
        _data = self.inhdl.read(_size)
        _duration = (perf_counter_ns() - _start) / 1024**3
        _num = len(_data)
        _mbsec = (_num / _duration) / 1024 / 1024

        if fromend:
            self._print(self.readformat.format(_num, '-', '-', _pos,
                                               _duration, _mbsec))
            self.csvwriter.writerow({'comment': '',
                                     'bytes': _num,
                                     'row': '-',
                                     'col': '-',
                                     'startpos': _pos,
                                     'duration': f'{_duration:>10.6f}',
                                     'MB/sec': f'{_mbsec:9.3f} MB/sec'
                                     })
        else:
            self._print(self.readformat.format(_num, self._row, self._col, _pos,
                                         _duration, _mbsec))
            self.csvwriter.writerow({'comment': '',
                                     'bytes': _num,
                                     'row': self._row,
                                     'col': self._col,
                                     'startpos': _pos,
                                     'duration': f'{_duration:>10.6f}',
                                     'MB/sec': f'{_mbsec:9.3f}'
                                     })

            # {:>10.6f} secs, {:9.3f} MB/sec')

    def read_pos(self, row=0, col=0, size=None):
        """
        Read a chunk of ``size`` from a given position.

        This method will first set the virtual cursor to row/col and then
        reads

        :param row:   the row
        :param col:   the column
        :param size:  the amount of bytes to read (or ``None`` for cell size)
        """
        self.pos(row, col)
        self.read(size=size)

    def straight(self, start_row=0, start_col=0, direction='right', count=5, overflow=False):
        """
        Return a list of positions (a list of tuples(row, column)).

        :param start_row:   start row
        :param start_col:   start col
        :param direction:   one of: "left", "right", "up", "down", "leftup",
                            "leftdown", "righup", "rightdown"
        :param count:       the number of steps requested
        :param overflow:    overflow to the opposite side (True/False)
        """
        if direction not in ["left", "right", "up", "down", "leftup",
                             "leftdown", "rightup", "rightdown"]:
            raise ValueError(f'invalid direction "{direction}"')

        srow = start_row
        scol = start_col
        ret = [(srow, scol)]

        for i in range(0, count-1):
            if direction == 'left':
                scol = scol-1 if scol else self.cols-1
            elif direction == 'right':
                scol = scol+1 if scol < self.cols-1 else 0
            elif direction == 'up':
                srow = srow-1 if srow else self.rows-1
            elif direction == 'down':
                srow = srow+1 if srow < self.rows-1 else 0
            elif direction == 'leftup':
                scol = scol-1 if scol else self.cols-1
                srow = srow-1 if srow else self.rows-1
            elif direction == 'leftdown':
                scol = scol-1 if scol else self.cols-1
                srow = srow+1 if srow < self.rows-1 else 0
            elif direction == 'rightup':
                scol = scol+1 if scol < self.cols-1 else 0
                srow = srow-1 if srow else self.rows-1
            elif direction == 'rightdown':
                scol = scol+1 if scol < self.cols-1 else 0
                srow = srow+1 if srow < self.rows-1 else 0

            ret.append((srow, scol))

        return ret

    def rectangle(self, row=0, col=0, width=5, height=5, filled=False):
        """
        Return a list of positions (a list of tuples(row, column)).

        :param row:     start row
        :param col:     start column
        :param width:   the width of the box
        :param height:  the height of the box
        :param filled:  just the frame (False) or all cells (True)
        """
        if col + width > self.cols or row + height > self.rows:
            raise ValueError(f'block doesn\'t fit into a map of {self.rows} x {self.cols}')

        ret = []

        for r in range(row, row+height):
            for c in range(col, col+width):
                if filled or c == col or c == col+width-1 or r == row or r == row+height-1:
                    ret.append((r,c))

        return ret

    def circle(self, row=50, col=50, radius=10, points=64, filled=False, start_middle=True):
        """
        Return a list of positions (a list of tuples(row, column)).

        :param row:           start row
        :param col:           start column
        :param radius:        the radius
        :param points:        the maximum no. of cells (reality will be smaller, likely)
        :param filled:        just the frame (False) or all cells (True)
        :param start_middle:  start with the most inner ring if True
        """

        def ring(radius, points):
            """
            sub-function to calculate a single ring.

            :param radius:  the radius
            :param points:  the maximum no. of cells (reality will be smaller, likely)
            :return:        a list of point, representing the ring
            """
            # raw point generation, might contain duplicates
            pts = [(int(cos(2 * pi / points * x) * radius), int(sin(2 * pi / points * x) * radius)) for x in
                   range(0, points + 1)]
            # convert to an OrderedSet to get rid of duplicates, and back to list
            return list(OrderedSet(pts))

        # prepare list of radius's inside-out or outside-in
        if start_middle:
            rads = range(1, radius+1)
        else:
            rads = range(radius, 0, -1)

        ret = []  # to collect the results (cells)
        for _rad in rads:
            # get the cells of a ring in real real coordinates
            cells = [(row+r,col+c) for r,c in ring(_rad, points)]
            if _rad == radius:
                for r, c in cells:
                    if r < 0 or r > self.rows - 1 or c < 0 or c > self.cols - 1:
                        raise ValueError(f'circle doesn\'t fit into a map of {self.rows} x {self.cols}')
                if not filled:  # stop here if filled is not wanted
                    ret += cells
                    break
            ret += cells

        return list(OrderedSet(ret))


    def _talkverbose(self, *args):
        """
        Internal method - print *args if self.verboise is True
        :param *args:  the arguments to print
        """
        if self._verbose:
            self._print(*args)
            self.csvwriter.writerow({'comment': ' '.join([str(x) for x in args]),
                                     'bytes': '',
                                     'row': '',
                                     'col': '',
                                     'startpos': '',
                                     'duration': '',
                                     'MB/sec': ''
                                     })

    def _print(self, *args, **kwargs):
        """
        Print to console, but also into a file for logging purposes.
        """
        print(*args, **kwargs)
        print(*args, **kwargs, file=self.logfile)

    def wait_for_return(self, message=''):
        """
        Print a message and wait for the user to press RETURN.
        """
        self.comment(message)
        input('press RETURN: ')
        print('...here we go:')

    def close(self, message='Done'):
        """
        Close the logfile.

        :param message:  a final message to be printed.
        """
        self.comment(message)
        self.logfile.close()
