# -*- coding: utf-8 -*-

# This file is closed source.
# Use without written allowance is prohibited.
#
# Copyright (c) 2017-2022 Thorsten Simons (sw@snomis.eu)

from setuptools import setup, find_packages
from codecs import open  # To use a consistent encoding
from os import path
from randiotestlib.version import Gvars

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, '../DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='randiotest',
    # Versions should comply with PEP440. For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # http://packaging.python.org/en/latest/tutorial.html#version

    version=str(Gvars.s_version),
    description=Gvars.Description,
    long_description=long_description,
    long_description_content_type = 'text/markdown',

    # The project's main homepage.
    url='https://none.yet',

    # Author details
    author=Gvars.Author,
    author_email=Gvars.AuthorMail,

    # Choose your license
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        # 3 - Alpha
        # 4 - Beta
        # 5 - Production/Stable
        'Development Status :: 5 - Production/Stable',

        # Indicate who your project is intended for
        'Intended Audience :: System Administrators',
        'Topic :: System :: Archiving',
        'Topic :: System :: Logging',

        # Pick your license as you wish (should match "license" above)
        'License :: Other/Proprietary License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',

        # more...
        'Operating System :: OS Independent',
        'Natural Language :: English',
    ],

    # What does your project relate to?
    keywords='random IO tests',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=[]),

    # List run-time dependencies here. These will be installed by pip when your
    # project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/technical.html#install-requires-vs-requirements-files
    install_requires=[],

    # List additional groups of dependencies here (e.g. development dependencies).
    # You can install these using the following syntax, for example:
    # $ pip install -e .[dev,test]
    extras_require = {},

    # If there are data files included in your packages that need to be
    # installed, specify them here. If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data={},

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages.
    # see http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    data_files=['DESCRIPTION.rst', 'DESCRIPTION.rst'],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the Target platform.
    entry_points={'console_scripts': ['randiotest = randiotestlib.__main__:main']}
)
