randiotest (|release|)
======================

**A tool to test read access patterns from cloud gateways**

When it comes to testing the responsiveness of caching cloud gateways, one
usually wants to know how specific access patterns are performing, beside of
simple full file copy operations.

Usually, a caching cloud gateway will hold files written to it in cache for
some time, while it will later on destage (or stub) the files to cloud
storage. While write access shouldn't be any big issue, some applications
require swift read performance, even if the file accessed is no more in the
cache. Applications often do random access to parts of files, instead of
reading it entirely.

This tool allows to define and run tests for such access patterns.


..  toctree::
    :maxdepth: 2

    15_installation
    20_run
    40_commands
    60_examples


