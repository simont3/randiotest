Installation
============

Python 3 >= 3.7 is required.

Create and activate a Python virtual environment::

    $ mkdir randiotest && cd randiotest
    $ python3 -m venv .venv
    $ source .venv/bin/activate
    $ pip install -U pip setuptools wheel

Install the ``randiotest`` package::

    $ pip install randiotest-<version>.tar.gz

