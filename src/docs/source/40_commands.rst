Methods usable in test procedure scripts
========================================

The ``Executor`` class will be available as **p** in test procedure scripts.
It's methods can be used as **p.<methodname>**.

..  Warning::

    ``p.setup()`` **needs** to be the first ``p``-method to be called -
    otherwise the test procedure script will fail.


..  autoclass:: randiotestlib.Executor
    :members:


