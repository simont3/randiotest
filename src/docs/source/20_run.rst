Run
===

Create a test procedure script fitting your needs (examples can be found in :doc:`60_examples`).
It should be stored in a file having the ``.py`` extension.

Run the test procedure script::

    (.venv) $ randiotest <test procedure script>.py
