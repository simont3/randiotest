Sample test scripts
===================

Simple routines
---------------

circle.py
+++++++++

Positions into the middle of the data file and then reads a centric blob of data.

..  code-block::

    from os.path import expanduser

    # setup step - 162x162 cells will result in a ~40 KiB cell size for a 1 GiB data file
    p.setup(file=expanduser('~/tmp/1GiB'), rows=162, cols=162)
    p.verbose(True)  # enable chatty output

    # read the first MiB from the data file
    p.comment('Read the first MiB from the data file')
    p.pos(row=0, col=0)
    p.read(size=1*1024**2)  # 1 MiB
    p.comment()

    # position in the middle of the grid and read a circular blob with a radius of 20 cells
    p.comment('Read a circular blob with a radius of 20 cells')
    coordinates = p.circle(row=81, col=81, radius=20, points=64, filled=True, start_middle=True)
    p.comment(f'Now reading {len(coordinates)} cells:')
    for row, col in coordinates:
        p.read_pos(row=row, col=col)

.. raw:: latex

    \clearpage

complete.py
+++++++++++

Read the entire file sequentially, cell by cell.

..  code-block::

    from os.path import expanduser

    # setup step - 162x162 cells will result in a ~40 KiB cell size for a 1 GiB data file
    p.setup(file=expanduser('~/tmp/1GiB'), rows=162, cols=162)

    p.comment(f'Read the entire data file as a rectangular blob, {p.rows} cells wide and {p.cols} cells high')
    # position to the first cell and read all cells
    coordinates = p.rectangle(row=0, col=0, width=162, height=162, filled=True)
    p.comment(f'Now reading {len(coordinates)} cells:')
    for row, col in coordinates:
        p.read_pos(row=row, col=col)

.. raw:: latex

    \clearpage

multi_rectangle.py
++++++++++++++++++

Read rectangular blobs of data from various positions in the data file.

..  code-block::

    from os.path import expanduser

    # setup step - 162x162 cells will result in a ~40 KiB cell size for a 1 GiB data file
    p.setup(file=expanduser('~/tmp/1GiB'), rows=162, cols=162)

    # read the first MiB from the data file
    p.comment('Read the first MiB from the data file')
    p.pos(row=0, col=0)
    p.read(size=1*1024**2)  # 1 MiB
    p.comment()

    # setup a list of position/sizes to read from
    # (a list of 6-tuples of (row, col, width, height, filled, title))
    bloblist = [(10, 10, 5, 5, True, 'upper left'),
                (10, 75, 5, 5, True, 'upper middle'),
                (10, 150, 5, 5, True, 'upper right'),
                (75, 10, 5, 5, True, 'middle left'),
                (75, 75, 5, 5, True, 'middle middle'),
                (75, 150, 5, 5, True, 'middle right'),
                (150, 10, 5, 5, True, 'lower left'),
                (150, 75, 5, 5, True, 'lower middle'),
                (150, 150, 5, 5, True, 'lower right'),
                ]
    p.comment(f'Reading {len(bloblist)} rectangular blobs')

    for row,col,width,height,filled,title in bloblist:
        # position in the grid and read a rectangular blob
        p.comment(f'Read a rectangular blob {"("+title+")" if title else ""}from {row}/{col}, {width} cells wide and {height} cells high')
        coordinates = p.rectangle(row=row, col=col, width=width, height=height, filled=filled)
        p.comment(f'Now reading {len(coordinates)} cells:')
        for row, col in coordinates:
            p.read_pos(row=row, col=col)

.. raw:: latex

    \clearpage

rectangle.py
++++++++++++

Read a rectangular blob of data from the data file.

..  code-block::

    from os.path import expanduser

    # setup step - 162x162 cells will result in a ~40 KiB cell size for a 1 GiB data file
    p.setup(file=expanduser('~/tmp/1GiB'), rows=162, cols=162)

    # read the first MiB from the data file
    p.comment('Read the first MiB from the data file')
    #p.wait_for_return()
    p.pos(row=0, col=0)
    p.read(size=1*1024**2)  # 1 MiB
    p.comment()

    # position in the middle of the grid and read a circular blob with a radius of 20 cells
    p.comment('Read a rectangular blob, 100 cells wide and 20 cells high')
    #p.wait_for_return()
    coordinates = p.rectangle(row=50, col=30, width=100, height=20, filled=True)
    p.comment(f'Now reading {len(coordinates)} cells:')
    for row, col in coordinates:
        p.read_pos(row=row, col=col)


.. raw:: latex

    \clearpage

Complex routines
----------------

complex.py
++++++++++

Perform a more complex read pattern:

    *   read 100 KiB starting at position 0
    *   read the last cell
    *   read 100 random cells
    *   read a sequence of cells, from a random cell into various directions
    *   finally, read the entire file

..  code-block::

    from os.path import expanduser

    # setup step - 162x162 cells will result in a ~40 KiB cell size for a 1 GiB data file
    p.setup(file=expanduser('~/tmp/1GiB'), rows=162, cols=162)
    p.comment()

    p.comment('--> Start with reading 100KB from the start of file')
    p.pos(0, 0)  # position to start of file (row = 0, col = 0)
    p.read(100 * p.KB)  # read 100KB
    p.comment()

    p.comment('--> Reading one cell from the end of the file')
    p.read(fromend=True)  # read the default block size
    p.comment()

    # read random cells
    _loop = range(100)
    _distance = 0

    p.comment('--> read random cells')
    for loop in _loop:
        p.pos_random(row_distance=_distance, col_distance=_distance)
        p.read()
    p.comment()

    # read a sequence of cells in various directions
    _loop = range(1)
    _distance = 3
    _reads = 25

    for mode, dir in [('Horizontal', 'right'),
                      ('Horizontal', 'left'),
                      ('Vertical', 'up'),
                      ('Vertical', 'down'),
                      ('Diagonal', 'leftup'),
                      ('Diagonal', 'leftdown'),
                      ('Diagonal', 'rightup'),
                      ('Diagonal', 'rightdown'),
                      ]:
        p.comment('--> {} straight tests - position randomly, then read straight {}'.format(mode, dir))
        for loop in _loop:
            p.pos_random(row_distance=_distance, col_distance=_distance)

            for row, col in p.straight(start_row=p._row, start_col=p._col, direction=dir, count=_reads):
                p.pos(row, col)
                p.read()
            p.comment()

    p.comment('--> Reading the entire file')
    p.pos(0, 0)  # position to the end
    p.read(p.infilesize)
    p.comment()

    p.comment('--> Finished')
